package com.example.ryanneilstroud.maps_1412;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RyanStroud on 20/12/14.
 */
public class MyFormFragment extends Fragment {

    private static final int REQUEST_TAKE_PICTURE = 0x01001;

    ImageView mImageView;
    Uri mImageUri;

    static Location mLoc = new Location("new location");

    Button takePictureButton;
    Button saveFormButton;

    String fileName = "com.example.ryanneilstroud.maps_1412_DATA_02";

    public static MyFormFragment newInstance(Location _loc) {
        MyFormFragment formFrag = new MyFormFragment();

        if(_loc == null){
            mLoc.setLongitude(0);
            mLoc.setLatitude(0);
        } else {
            mLoc = _loc;
        }

        return formFrag;
    }

    @Override
    public void onActivityCreated(Bundle _savedInstanceState) {
        super.onActivityCreated(_savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.form_my, container, false);

        mImageView = (ImageView)rootView.findViewById(R.id.takePicture_ImageView);

        takePictureButton = (Button)rootView.findViewById(R.id.takePicture_Button);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                mImageUri = getImageUri();
                if(mImageUri != null) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                }
                startActivityForResult(intent, REQUEST_TAKE_PICTURE);
            }
        });

        saveFormButton = (Button)rootView.findViewById(R.id.saveForm_Button);
        saveFormButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //get R
                EditText locationName = (EditText)rootView.findViewById(R.id.nameOfLocation_EditText);
                EditText locationComments = (EditText)rootView.findViewById(R.id.comments_EditText);
                //put R in JSON Object
                if(!locationName.getText().toString().matches("") && !locationComments.getText().toString().matches("") && mImageUri != null){

                    File file = getActivity().getFileStreamPath(fileName);
                    if(file.exists()){
                        System.out.println("FILE EXISTS");

                        String s = readFromFile(fileName);

                        JSONObject jsonObj = null;

                        try {
                            jsonObj = new JSONObject(s.substring(s.indexOf("{"), s.lastIndexOf("}") + 1));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("ERROR (JSONException): " + jsonObj);
                        }

                        String[] innerObjStr = {"NAMES", "COMMENTS", "IMAGES", "LATS", "LONS"};

                        Object[] myObj = {locationName.getText(), locationComments.getText(), mImageUri, mLoc.getLatitude(), mLoc.getLongitude()};

                        for (int i = 0; i < innerObjStr.length; i++) {
                            try {
                                jsonObj.getJSONObject(innerObjStr[i]).put(innerObjStr[i].toLowerCase().substring(0, innerObjStr[i].length()-1) + jsonObj.getJSONObject(innerObjStr[i]).length(), myObj[i]);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        writeToFile(jsonObj.toString(), fileName);

                    } else {
                        System.out.println("FILE DOES NOT EXIST");

                        //put data in JSON object
                        JSONObject newObj = new JSONObject();

                        String[] innerObjStr = {"NAMES", "COMMENTS", "IMAGES", "LATS", "LONS"};

                        for (int i = 0; i < innerObjStr.length; i++) {
                            try {
                                newObj.put(innerObjStr[i], new JSONObject());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        System.out.println("mImageUri: " + mImageUri);
                        Object[] myObj = {locationName.getText(), locationComments.getText(), mImageUri, mLoc.getLatitude(), mLoc.getLongitude()};

                        for (int i = 0; i < innerObjStr.length; i++) {
                            try {
                                newObj.getJSONObject(innerObjStr[i]).put(innerObjStr[i].toLowerCase().substring(0, innerObjStr[i].length()-1) + 0, myObj[i]);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        //write file
                        writeToFile(newObj.toString(), fileName);
                    }

                    MyMapFragment frag = new MyMapFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, frag)
                            .commit();
                } else {
                    Toast.makeText(getActivity(), "input data", Toast.LENGTH_SHORT).show();
                }
                //save JSON Object
            }
        });

        return rootView;
    }

    //SAVE LOCAL FILE
    public void writeToFile(String result, String fileName) {
        FileOutputStream fos = null;
        try {

            fos = getActivity().openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(result);
            os.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("FileNotFoundException");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IOException");
        }
    }

    //READ LOCAL FILE
    public String readFromFile(String fileName) {

        String ret = "";

        try {
            InputStream inputStream = getActivity().openFileInput(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException");
        } catch (IOException e) {
            System.out.println("IOException");
        }

        return ret;
    }

    private Uri getImageUri() {

        String imageName = new SimpleDateFormat("MMddyyyy_HHmmss").format(new Date(System.currentTimeMillis()));

        File imageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File appDir = new File(imageDir, "CameraExample");
        appDir.mkdirs();

        File image = new File(appDir, imageName + ".jpg");
        try {
            image.createNewFile();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }

        return Uri.fromFile(image);
    }

    private void addImageToGallery(Uri imageUri) {
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        scanIntent.setData(imageUri);
        getActivity().sendBroadcast(scanIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_TAKE_PICTURE) {
            if(mImageUri != null) {
                mImageView.setImageBitmap(BitmapFactory.decodeFile(mImageUri.getPath()));
                addImageToGallery(mImageUri);
            } else {
                mImageView.setImageBitmap((Bitmap)data.getParcelableExtra("data"));
            }
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.setGroupEnabled(0, false);
    }
}
