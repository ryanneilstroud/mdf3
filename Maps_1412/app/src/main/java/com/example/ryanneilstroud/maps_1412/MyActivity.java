package com.example.ryanneilstroud.maps_1412;

import android.app.Activity;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Created by RyanStroud on 20/12/14.
 */

public class MyActivity extends Activity {

    LocationManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        mManager = (LocationManager)getSystemService(LOCATION_SERVICE);

        MyMapFragment frag = MyMapFragment.newInstance(mManager);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, frag)
                .commit();
    }
}
