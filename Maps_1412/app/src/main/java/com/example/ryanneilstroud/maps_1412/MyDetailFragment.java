package com.example.ryanneilstroud.maps_1412;

import android.app.Fragment;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by RyanStroud on 20/12/14.
 */
public class MyDetailFragment extends Fragment{

    String fileName = "com.example.ryanneilstroud.maps_1412_DATA_02";

    static int mIndex;

    public static MyDetailFragment newInstance(int _index) {
        MyDetailFragment frag = new MyDetailFragment();

        mIndex = _index;

        return frag;
    }

    @Override
    public void onActivityCreated(Bundle _savedInstanceState) {
        super.onActivityCreated(_savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.detail_my, container, false);

        File file = getActivity().getFileStreamPath(fileName);
        if(file.exists()){
            String s = readFromFile(fileName);

            JSONObject obj = null;

            try {
                obj = new JSONObject(s.substring(s.indexOf("{"), s.lastIndexOf("}") + 1));
                System.out.println("obj + MyDetailFragment: " + obj);
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("UNABLE TO CONVERT STRING TO JSON: " + obj);
            }

            TextView name = (TextView)rootView.findViewById(R.id.location_name_detail);
            TextView comment = (TextView)rootView.findViewById(R.id.location_comments_detail);
            ImageView image = (ImageView)rootView.findViewById(R.id.location_image_detail);

            try {
                name.setText(obj.getJSONObject("NAMES").getString("name" + mIndex));
                comment.setText(obj.getJSONObject("COMMENTS").getString("comment" + mIndex));

                Uri mImageUri = Uri.parse(obj.getJSONObject("IMAGES").getString("image" + mIndex));
                image.setImageBitmap(BitmapFactory.decodeFile(mImageUri.getPath()));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return rootView;
    }

    //READ LOCAL FILE
    public String readFromFile(String fileName) {

        String ret = "";

        try {
            InputStream inputStream = getActivity().openFileInput(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException");
        } catch (IOException e) {
            System.out.println("IOException");
        }

        return ret;
    }
}
