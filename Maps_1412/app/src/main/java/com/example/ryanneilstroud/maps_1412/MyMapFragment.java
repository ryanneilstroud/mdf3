package com.example.ryanneilstroud.maps_1412;

/**
 * Created by RyanStroud on 20/12/14.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MyMapFragment extends MapFragment implements OnInfoWindowClickListener, GoogleMap.OnMapLongClickListener, LocationListener {

    private static final int REQUEST_ENABLE_GPS = 0x02001;
    private static final String LAST_KNOWN_LOCATION = "Last Known Location";

    static ArrayList<String> mNames = null;
    static ArrayList<Double> mLats = new ArrayList<Double>();

    String fileName = "com.example.ryanneilstroud.maps_1412_DATA_02";

    File file = null;

    GoogleMap mMap;
    static LocationManager mManager;
    Location mLoc;

    public static MyMapFragment newInstance(LocationManager _manager) {
        MyMapFragment mapFrag = new MyMapFragment();

        mManager = _manager;

        return mapFrag;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);

        mMap = getMap();
        mMap.setInfoWindowAdapter(new MarkerAdapter());
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMapLongClickListener(this);

        file = getActivity().getFileStreamPath(fileName);
        if(file.exists()){
            String s = readFromFile(fileName);

            JSONObject obj = null;
            mNames = new ArrayList<String>();

            try {
                obj = new JSONObject(s.substring(s.indexOf("{"), s.lastIndexOf("}") + 1));
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("UNABLE TO CONVERT STRING TO JSON: " + obj);
            }

            try {
                for(int i = 0; i < obj.getJSONObject("LATS").length(); i++){
                    Location loc = new Location("new location");
                    loc.setLatitude(Double.parseDouble(obj.getJSONObject("LATS").getString("lat" + i)));
                    loc.setLongitude(Double.parseDouble(obj.getJSONObject("LONS").getString("lon" + i)));

                    String name = obj.getJSONObject("NAMES").getString("name" + i);
                    mNames.add(name);
                    mLats.add(loc.getLatitude());

                    addMarker(loc, name);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        enableGps();
    }

    private void enableGps() {
        if(mManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            mManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 10, this);

            Location loc = mManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(loc != null) {
                mLoc = loc;

                if(file.exists()){
                    for (int i = 0; i < mLats.size(); i++){
                        if(mLats.get(i) != loc.getLatitude()){
                            addMarker(loc, LAST_KNOWN_LOCATION);
                        }
                    }
                } else {
                    addMarker(loc, LAST_KNOWN_LOCATION);
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 16));
            }
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle("GPS Unavailable")
                    .setMessage("Please enable GPS in the system settings.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(settingsIntent, REQUEST_ENABLE_GPS);
                        }

                    })
                    .show();
        }
    }

    private void addMarker(Location _loc, String _name) {
        mMap.addMarker(new MarkerOptions().position(new LatLng(_loc.getLatitude(),_loc.getLongitude())).title(_name));
    }

    @Override
    public void onInfoWindowClick(final Marker marker) {
        if(marker.getTitle().equals(LAST_KNOWN_LOCATION)){
            new AlertDialog.Builder(getActivity())
                    .setTitle("Marker Clicked")
                    .setMessage("You clicked on: " + marker.getTitle())
                    .setPositiveButton("Close", null)
                    .show();
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Marker Clicked")
                    .setMessage("You clicked on: " + marker.getTitle())
                    .setPositiveButton("Close", null)
                    .setNegativeButton("View", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            int index = 0;

                            for (int i = 0; i < mNames.size(); i++) {
                                if (mNames.get(i).equals(marker.getTitle())) {
                                    System.out.println("mNames: " + mNames);
                                    index = i;
                                }
                            }

                            mNames = null;

                            MyDetailFragment frag = MyDetailFragment.newInstance(index);
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.container, frag)
                                    .commit();
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        Location selectLoc = new Location("Selected Location");
        selectLoc.setLatitude(latLng.latitude);
        selectLoc.setLongitude(latLng.longitude);

        MyFormFragment frag = MyFormFragment.newInstance(selectLoc);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, frag)
                .commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.add:
                MyFormFragment frag = MyFormFragment.newInstance(mLoc);
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, frag)
                        .commit();
                return true;
            case R.id.update:
                enableGps();
                Toast.makeText(getActivity(), "Location Updated", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.delete:
                File file = getActivity().getFileStreamPath(fileName);
                if(file.exists()){
                    file.delete();

                    Toast.makeText(getActivity(), "File Deleted", Toast.LENGTH_SHORT).show();

                    MyMapFragment mapFrag = new MyMapFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, mapFrag)
                            .commit();
                } else {
                    Toast.makeText(getActivity(), "Nothing To Delete", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //READ LOCAL FILE
    public String readFromFile(String fileName) {

        String ret = "";

        try {
            InputStream inputStream = getActivity().openFileInput(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException");
        } catch (IOException e) {
            System.out.println("IOException");
        }

        return ret;
    }

    @Override
    public void onLocationChanged(Location location) {
        addMarker(location, LAST_KNOWN_LOCATION);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class MarkerAdapter implements InfoWindowAdapter {

        TextView mText;

        public MarkerAdapter() {
            mText = new TextView(getActivity());
        }

        @Override
        public View getInfoContents(Marker marker) {
            mText.setText(marker.getTitle());
            return mText;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }
    }
}
